package com.jfinal.ext.log;


import com.jfinal.log.ILogFactory;
import com.jfinal.log.Logger;

/**
 * ILogFactoryExt.
 */
public interface ILogFactoryExt extends ILogFactory {
	
	Logger getLogger(Class<?> clazz);
	
	Logger getLogger(String name);
}